def triangle_sierpinski(tortue, n, l):
    """
    Fonction qui créer un tiangle de Sierpinski
    paramètre: n = int, le nombre de fois qu'on veut repéter
               l = la longueur iniiale des coté du triangle
    return: None
    """
    if n != 0:
        for _ in range(3):
            triangle_sierpinski(tortue, n - 1, l / 2)
            tortue.avancer(l)
            tortue.tourner_gauche(120)


def mosaiqueSerpinski(tortue, n, l=0, i=2):
    """
    Créer une mosaîque grace a des triangles de Serpinski
    paramètre par defaut: l = 0
                          i = 2
                          n = int, le nombre de fois qu'on veut repéter
    return: None
    """
    tortue.relever_stylo()
    tortue.aller_a_xy(500, 300)
    tortue.abaisser_stylo()
    if n != 0:
        if n % 2 == 0:
            tortue.tourner_gauche(60)
            triangle_sierpinski(tortue, 6, l)
        mosaiqueSerpinski(tortue, n - 1, l)

import arbre_binaire
from math import cos, sin


def tete(tortue, x, y, r):
    """
    fonction qui permet de créer des cercles
    :param tortue (TortueBasique): la tortue
    :param x (int): le centre x
    :param y: le centre y
    :param r(int): le rayon
    """
    cosS = [cos(x) * r for x in range(100)]
    sinS = [sin(x) * r for x in range(100)]

    tortue.relever_stylo()
    tortue.aller_a_xy(x + r, y)
    tortue.abaisser_stylo()

    for i in range(len(cosS)):
        tortue.aller_a_xy(cosS[i] + x, sinS[i] + y)


def cheveux(tortue, x, y, longueur):
    """
    fonction qui permet de créer des cheveux
    :param tortue (TortueBasique): la tortue
    :param x (int): le  point x
    :param y: le point y
    :param r(int): la longeur des cheveux
    """
    cosS = [cos(0.1 * x + 20) * 10 for x in range(longueur)]
    tortue.relever_stylo()
    tortue.aller_a_xy(x, y)
    tortue.abaisser_stylo()
    for xx in range(longueur):
        tortue.aller_a_xy(cosS[xx] + x, xx + y)


def draw(tortueHelene, tortueJibril, vesqui=False):
    """
    Fonction qui permet de dessiner deux personnages: Hélène et Jibril
    :param tortueHelene: la tortue qui dessine ln.
    :param tortueJibril: la tortue qui dessine jibril.
    """
    tortueHelene.changer_epaisseur(1), tortueJibril.changer_epaisseur(1)
    tortueHelene.changer_couleur("white")
    tortueHelene.relever_stylo()
    tortueHelene.aller_a_xy(140, 400)
    tortueHelene.tourner_droite(90)

    tortueHelene.relever_stylo()
    tortueHelene.avancer(40)
    tortueHelene.tourner_gauche(120)
    tortueHelene.abaisser_stylo()
    tortueHelene.avancer(50)
    tortueHelene.relever_stylo()
    tortueHelene.aller_a_xy(140, 400)
    tortueHelene.angle = 0
    tortueHelene.tourner_droite(90)

    tortueHelene.abaisser_stylo()
    tortueHelene.avancer(100)
    tortueHelene.angle = 0
    arbre_binaire.arbre_bin(tortueHelene, 2, 100)
    tortueHelene.relever_stylo()
    tete(tortueHelene, 140, 370, 30)
    tete(tortueHelene, 130, 370, 5)
    tete(tortueHelene, 160, 370, 5)
    tortueHelene.couleur = "brown"
    tortueHelene.epaisseur = 5
    cheveux(tortueHelene, 120, 350, 70)
    cheveux(tortueHelene, 130, 345, 70)
    cheveux(tortueHelene, 160, 350, 70)
    cheveux(tortueHelene, 150, 350, 70)

    tortueJibril.relever_stylo()
    if vesqui:
        tortueJibril.aller_a_xy(800, 420)
    else:
        tortueJibril.aller_a_xy(800, 400)
    tortueJibril.tourner_droite(90)
    tortueJibril.abaisser_stylo()
    if vesqui:
        tortueJibril.relever_stylo()
        tortueJibril.avancer(75)
        tortueJibril.abaisser_stylo()
        tortueJibril.avancer(200)
    else:
        tortueJibril.avancer(100)
    tortueJibril.angle = 0
    arbre_binaire.arbre_bin(tortueJibril, 2, 100)
    tortueJibril.relever_stylo()
    if vesqui:
        tete(tortueJibril, 800, 470, 30)
        tete(tortueJibril, 780, 470, 5)
        tete(tortueJibril, 810, 470, 5)
    else:
        tete(tortueJibril, 800, 370, 30)
        tete(tortueJibril, 780, 370, 5)
        tete(tortueJibril, 810, 370, 5)
    tortueJibril.couleur = "white"
    tortueJibril.epaisseur = 4
    if vesqui:
        cheveux(tortueJibril, 770 + 10, 420, 30)
        cheveux(tortueJibril, 780 + 10, 420, 30)
        cheveux(tortueJibril, 790 + 10, 420, 30)
        cheveux(tortueJibril, 800 + 10, 420, 30)
    else:
        cheveux(tortueJibril, 770 + 10, 350, 30)
        cheveux(tortueJibril, 780 + 10, 340, 30)
        cheveux(tortueJibril, 790 + 10, 340, 30)
        cheveux(tortueJibril, 800 + 10, 350, 30)

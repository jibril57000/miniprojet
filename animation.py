import scene, koch, colors, sierpinski
from tkinter import ALL, Label


global vesqui, i
vesqui = False


def neige(fenetre, tortue, tortueHelene, tortueJibril, x=0, angle=0, point=0, pointH=0):
    """
    fonction qui anime le jeu jibril vs hélène
    permet d'envoyer des boules de neige et de dessiner la scène

    :param fenetre: la fenetre:
    :param tortue: la tortue qui dessine la scène
    :param tortueHelene la tortue qui dessine  ln:
    :param tortueJibril la tortue qui dessine jibril:
    :param x=0: tous les 0.024 secondes ajoute 10 à x
    :param angle=0: fait bug le programme alors reste comme ça
    :param point: les points de jibril
    :param pointH: les points de LN
    """

    point = point

    # Création du texte du score de Jibril
    l = Label(fenetre, text=f"Score Jibril: {point}", bg="black", fg="white", font=20)
    l.place(relx=0.75, rely=0.5)

    # Création du texte du score de LN
    lH = Label(fenetre, text=f"Score Helene: {pointH}", bg="black", fg="white", font=20)
    lH.place(relx=0.1, rely=0.5)

    # On supprime l'image précédente
    tortue.zone_de_dessin.delete(ALL)
    # On dessine une nouvelle scène
    scene.draw(tortueHelene, tortueJibril, vesqui)
    tortue.relever_stylo()
    tortue.aller_a_xy(x, 360)
    tortue.relever_stylo()
    tortue.changer_couleur("pink")
    koch.etoileKoch(tortue, 3, 50)
    tortue.angle = angle
    # On vérifie si au moment où l'etoile me depasse je suis abaissé si oui +1 point
    if x > 800:
        tortue.relever_stylo()
        tortue.aller_a_xy(140, 400)
        tortue.abaisser_stylo()
        if vesqui == True:
            point += 1
            pointH -= 1
        else:
            pointH += 1
            point -= 1

    # On rappel la fonction avec les paramètres adéquats
    fenetre.after(
        24,
        neige,
        fenetre,
        tortue,
        tortueHelene,
        tortueJibril,
        tortue.coordonnees.x + 50,
        angle,
        point,
        pointH,
    )


def illusionZoom(tortue, fenetre, l, color=0, angle=0):
    """
    Création d'une anmation illusion qui zoom et change la couleur et qui tourne !
    :param tortue: la tortue
    :param fenetre: la fenetre:
    :param l (int): la longueur qui change
    :param color: on passe à la couleur suivante à chaque appel de la fonction
    :param angle: on change l'angle à chaque appel
    """

    # Si la taille dépasse la zone de dessin on fait repartir dans l'autre sens
    if l > 1200:
        l = -1200
    # Si on arrive à la fin du dégradé on revient au début
    elif color == len(colors.fade()):
        color = 0
    # On supprime l'image précédente
    tortue.zone_de_dessin.delete(ALL)
    tortue.relever_stylo()
    tortue.aller_a_xy(0, 0)
    tortue.abaisser_stylo()
    tortue.changer_epaisseur(1)
    tortue.angle = angle
    tortue.changer_couleur(colors.fade()[color])
    sierpinski.mosaiqueSerpinski(tortue, 12, l)
    # On rappel la fonction avec les paramètres adéquats
    zoom = fenetre.after(
        1, illusionZoom, tortue, fenetre, l + 10, color + 10, angle + 0.009
    )

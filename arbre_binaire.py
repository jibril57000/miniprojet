def arbre_bin(tortue, n, l):
    """
    Création d'un arbre binaire
    paramètre: n: nombre de branche
               l: logueur des branches
    """
    if n != 1:

        # sous arbre droit
        x, y = tortue.coordonnees.x, tortue.coordonnees.y
        tortue.tourner_droite(60)
        tortue.avancer(l)
        tortue.angle = 0
        arbre_bin(tortue, n - 1, l / 2)


        # on revient aux coordonnées de départ
        tortue.coordonnees.x, tortue.coordonnees.y = x, y

        # sous arbre gauche
        tortue.tourner_droite(120)
        tortue.avancer(l)
        tortue.angle = 0
        arbre_bin(tortue, n - 1, l / 2)

from math import *


class Position:
    """Création de l'objet position"""

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class TortueBasique:
    """Création de l'objet Tortue"""

    def __init__(self, coul, zone_de_dessin, pos=Position()):
        self.coordonnees = pos
        self.statut = True
        self.couleur = coul
        self.epaisseur = 1
        self.angle = 0
        self.zone_de_dessin = zone_de_dessin

    def aller_a_xy(self, x, y):
        """
        Méthode permettant de se déplacer au point de coordonnnées passées en paramètres
        :param x: int ou float, abscisse du point d'arrivée
        :param y: int ou float, ordonnée du point d'arrivée
        :return: None
        """

        if self.statut == True:
            self.zone_de_dessin.create_line(
                self.coordonnees.x,
                self.coordonnees.y,
                x,
                y,
                fill=self.couleur,
                width=self.epaisseur,
            )
        self.coordonnees.x = x
        self.coordonnees.y = y

    def relever_stylo(self):
        """
        Méthode permettant de relever le stylo et ne pas tracer lors du déplacement
        :return: None
        """

        self.statut = False

    def abaisser_stylo(self):
        """
        Méthode permettant d'abaisser le stylo pour pouvoir tracer un segment
        :return: None
        """

        self.statut = True

    def changer_couleur(self, couleur):
        """
        Méthode permettant de modifier la couleur du stylo
        :return: None
        """

        self.couleur = couleur

    def changer_epaisseur(self, val):
        """
        Méthode permettant de modifier l'épaisseur du stylo
        :return: None
        """

        self.epaisseur = val

    def tourner_gauche(self, angle_deg):
        """
        Méthode permettant d'orienter le tracer vers la gauche suivant un angle
        :param angle_deg : int ou float, angle en degrés de la rotation
        :return : None
        """

        self.angle -= angle_deg / 180 * pi

    def tourner_droite(self, angle_deg):
        """
        Méthode permettant d'orienter le tracer vers la droite suivant un angle
        :param angle_deg: int ou float, angle en degrés de la rotation
        ;return  None
        """

        self.angle += angle_deg / 180 * pi

    def avancer(self, longueur):
        """
        Méthode permettant d'avancer d'une longueur fixée suivant un angle fixé
        :param longueur: int ou float, longueur en pixels du déplacement
        :return :
        """

        x = self.coordonnees.x + longueur * cos(self.angle)
        y = self.coordonnees.y + longueur * sin(self.angle)

        self.aller_a_xy(x, y)

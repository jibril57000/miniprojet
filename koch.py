import random, colors


def koch(tortue, n, l):
    """
    fonction qui permet de creer un flocon de koch
    paramètre: tortue:
                n: int: nombre de fois qu'on veut dviser l
                l: int, longueur des cotés
    """
    if n == 0:
        tortue.abaisser_stylo()
        tortue.avancer(l)
    else:
        koch(tortue, n - 1, l / 3)
        tortue.tourner_gauche(60)
        koch(tortue, n - 1, l / 3)
        tortue.tourner_droite(120)
        koch(tortue, n - 1, l / 3)
        tortue.tourner_gauche(60)
        koch(tortue, n - 1, l / 3)


def etoileKoch(tortue, n, l):
    """
    Creéation de l'étoile de Koch complète
    n: int: nombre de fois qu'on veut dviser l
                l: int, longueur des cotés
    """
    tortue.tourner_droite(60)
    koch(tortue, n, l)
    tortue.tourner_droite(120)
    koch(tortue, n, l)
    tortue.tourner_droite(120)
    koch(tortue, n, l)


color1, color2 = random.choice(colors.fade()), random.choice(colors.fade())


def infiniteStar(tortue, n, l=0):
    """
    fonction qui créer des etoiles à l'infini
    n: int: nombre de fois qu'on veut dviser l
                l = 0
    """
    tortue.relever_stylo()
    if n % 2 == 0:
        tortue.changer_couleur(color1)
    else:
        tortue.changer_couleur(color2)
    tortue.aller_a_xy(500, 250)
    tortue.angle = 0
    x, y = tortue.coordonnees.x, tortue.coordonnees.y
    if n != 0:
        tortue.aller_a_xy(tortue.coordonnees.x, tortue.coordonnees.y - l / 1.7)
        etoileKoch(tortue, 6, l)
        infiniteStar(tortue, n - 1, l / 1.7)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 21:28:29 2020

@author: louisvincent
"""

from tkinter import *
from math import *
from tortue_basique import *

# importation de koch
import koch, animation, sierpinski, arbre_binaire, random, pendule


def changerTrue(value):
    animation.vesqui = value


# =============================================================================
# PROGRAMME PRINCIPAL
# =============================================================================

"""
Fonction tkinter
"""
fenetre = Tk()
zone_de_dessin = Canvas(width=1000, height=600, background="black")
zone_de_dessin.pack()

"""
Création de notre artiste
"""
tortue = TortueBasique("white", zone_de_dessin)
tortue.relever_stylo()
tortue.aller_a_xy(200, 300)
tortue.abaisser_stylo()

"""
Partie 1 l'étoile de Koch
*** /!\ faire attention aux valeurs de n c'est quand on perd son macbook qu'on se rend compte de sa valeur ***

4 fonctions:
koch         -> l'etoile de koch
etoileKoch   -> le flocon
infiniteStar -> flocon qui se repete dans un flocon (recursif)
animation    -> bataille de boules (flocon) de neige (koch)
animation est un jeu où il faut esquiver les boules de neige avec la flèche du bas
"""

"""
Création des deux tortues pour la bataille 
"""
tortueHelene, tortueJibril = TortueBasique("white", zone_de_dessin), TortueBasique(
    "white", zone_de_dessin
)


# Flocon de Koch
#koch.koch(tortue,6,200)
#koch.etoileKoch(tortue,6,200)
#koch.infiniteStar(tortue,6,1000)

# Le jeu
#fenetre.bind("<Down>", lambda event: changerTrue(True))
#fenetre.bind("<KeyRelease-Down>", lambda event: changerTrue(False))
#animation.neige(fenetre,tortue,tortueHelene,tortueJibril)


"""
Partie 2 triangle de Serpinski
*** /!\ faire attention aux valeurs de n c'est quand on perd son macbook qu'on se rend compte de sa valeur ***

triangle_sierpkinski -> le triangle de base
mosaiqueSerpinski    -> Une jolie mosaique
illusionZoom         -> une jolie animation
"""

#sierpinski.triangle_sierpinski(tortue,7,300)
#sierpinski.mosaiqueSerpinski(tortue,12,300)
#animation.illusionZoom(tortue,fenetre,100)

"""
Partie 3 arbre binaire et pendule
*** /!\ faire attention aux valeurs de n c'est quand on perd son macbook qu'on se rend compte de sa valeur ***

arbre_binaire -> l'arbre binaire 
pendule       -> un pendule simple avec un angle de base de pi/2 et l'accélération terrestre (g=9.81) en utilisant l'équation diff ∂²θ/∂t² + w(0)²θ = 0
"""

# On replace notre tortue
# tortue.relever_stylo(),tortue.aller_a_xy(500,0),tortue.abaisser_stylo()
# arbre_binaire.arbre_bin(tortue,6,300)
#pendule.pendule(tortue,fenetre)

fenetre.mainloop()

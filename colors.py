def fade():
    """
    fonction qui permet d'avoir un dégradé de couleur
    """
    red255 = ["#%02x%02x%02x" % (255, i, 0) for i in range(255)]
    green255 = ["#%02x%02x%02x" % (255 - i, 255, 0) for i in range(255)]
    blue255 = ["#%02x%02x%02x" % (0, 255, i) for i in range(255)]
    green0 = ["#%02x%02x%02x" % (0, 255 - i, 255) for i in range(255)]
    redto255 = ["#%02x%02x%02x" % (i, 0, 255) for i in range(255)]
    bto0 = ["#%02x%02x%02x" % (255, 0, 255 - i) for i in range(255)]

    return red255 + green255 + blue255 + green0 + redto255 + bto0

from scene import tete
from tkinter import ALL
import numpy as np
from math import *

"""
** Pendule simple en utilisant l'équation différentielle
            * ∂²θ/∂t² + w(0)²θ = 0 *
            dont une solution est:
            θ(t) = θ(0) * sin(w(0) *t)


j'ai voulu essayer de faire un double pendule les équations sont illisbles :(
"""


"""
Les constantes méthématiques
"""
ANGLE_DEPART = pi / 4
G = 9.81


def theta(tetha0, t, l):
    """
    Fonction solution de l'équation différentielle
    ∂²θ/∂t² + w(0)²θ = 0
    :param theta0 (real): l'angle de base
    :param t (real): l'instant t
    :param l: la longueur du pendule en m
    """
    return tetha0 * sin(t * 1 / (sqrt(l / G)))


"""
Création d'une liste contenant les angles
La fonction sinus est 2 pi périodique
"""
thetat = [theta(ANGLE_DEPART, i, 40) for i in list(np.arange(-2 * pi, 2 * pi, 0.01))]


def pendule(tortue, fenetre, angle=0):
    """
    Creation de l'animation en utilisant .after (TKINTER)
    :param tortue (TortueBasique): la tortue pour dessiner
    :param fenetre: la fenetre Tkinter
    :param angle=0: l'angle théta calculé
    """
    if angle == len(thetat):
        angle = 0
    tortue.zone_de_dessin.delete(ALL)
    tortue.relever_stylo()
    tortue.aller_a_xy(500, 100)
    tortue.abaisser_stylo()
    tortue.angle = thetat[angle] + pi / 2
    tortue.avancer(200)
    tete(tortue, tortue.coordonnees.x, tortue.coordonnees.y + 7, 10)
    fenetre.after(1, pendule, tortue, fenetre, angle + 1)
